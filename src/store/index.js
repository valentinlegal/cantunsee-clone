import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		levels: [
			{
				name: 'Tutorial',
				points: 10,
				color: '#0098FF',
				questions: [
					{ choix: ['img1', 'img2'], reponse: 'img1', help: 'Description will appear here' },
					{ choix: ['img3', 'img4'], reponse: 'img4', help: 'Icons, fonts, shapes' },
					{ choix: ['img5', 'img6'], reponse: 'img6', help: 'Speech bubble padding'},
				],
			},
			{
				name: 'Easy',
				points: 100,
				color: '#0098FF',
				questions: [
					{ choix: ['img7', 'img8'], reponse: 'img7', help: 'Photo aspect ratio' },
					{ choix: ['img9', 'img10'], reponse: 'img10', help: 'Icon size' },
					{ choix: ['img11', 'img12'], reponse: 'img11', help: 'Price tag contrast' },
					{ choix: ['img13', 'img14'], reponse: 'img13', help: 'Border radius' },
					{ choix: ['img15', 'img16'], reponse: 'img16', help: 'Photo aspect ratio' },
					{ choix: ['img17', 'img18'], reponse: 'img17', help: 'Capitalization' },
					{ choix: ['img19', 'img20'], reponse: 'img19', help: 'One button with default style' },
					{ choix: ['img21', 'img22'], reponse: 'img22', help: 'Subtitle text contrast' },
					{ choix: ['img23', 'img24'], reponse: 'img23', help: 'Activity indicator color' },
					{ choix: ['img25', 'img26'], reponse: 'img26', help: 'Separator width' },
					{ choix: ['img27', 'img28'], reponse: 'img27', help: 'Consistent icons style' },
					{ choix: ['img29', 'img30'], reponse: 'img29', help: 'Icon contrast' },
					{ choix: ['img31', 'img32'], reponse: 'img32', help: 'Button caption contrast' },
					{ choix: ['img33', 'img34'], reponse: 'img33', help: 'Placeholder text contrast' },
					{ choix: ['img35', 'img36'], reponse: 'img35', help: 'Button background contrast' },
					{ choix: ['img37', 'img38'], reponse: 'img38', help: 'Vertical text alignment' },
					{ choix: ['img39', 'img40'], reponse: 'img40', help: 'Destructive action button color' },
					{ choix: ['img41', 'img42'], reponse: 'img42', help: 'Placeholder text capitalization' },
				],
			},
			{
				name: 'Medium',
				points: 150,
				color: '#851CFF',
				questions: [
					{ choix: ['img43', 'img44'], reponse: 'img44', help: 'Subtitle offset' },
					{ choix: ['img45', 'img46'], reponse: 'img45', help: 'Contrast' },
					{ choix: ['img47', 'img48'], reponse: 'img47', help: 'Spacing between title and subtitle' },
					{ choix: ['img49', 'img50'], reponse: 'img49', help: 'Icon vertical alignment' },
					{ choix: ['img51', 'img52'], reponse: 'img51', help: 'Button background' },
					{ choix: ['img53', 'img54'], reponse: 'img53', help: 'Text wrapping' },
					{ choix: ['img55', 'img56'], reponse: 'img56', help: 'Skip button capitalization' },
					{ choix: ['img57', 'img58'], reponse: 'img58', help: 'Spacing between text blocks' },
					{ choix: ['img59', 'img60'], reponse: 'img59', help: 'Placeholder text tracking' },
					{ choix: ['img61', 'img62'], reponse: 'img61', help: 'Vertical button alignment' },
					{ choix: ['img63', 'img64'], reponse: 'img63', help: 'Font weight' },
					{ choix: ['img65', 'img66'], reponse: 'img65', help: 'Icon tint color' },
					{ choix: ['img67', 'img68'], reponse: 'img67', help: 'Image resolution' },
					{ choix: ['img69', 'img70'], reponse: 'img70', help: 'Price tag alignment' },
					{ choix: ['img71', 'img72'], reponse: 'img72', help: 'Border radius' },
					{ choix: ['img73', 'img74'], reponse: 'img74', help: 'Search icon size' },
					{ choix: ['img75', 'img76'], reponse: 'img76', help: 'Vertical content alignment' },
					{ choix: ['img77', 'img78'], reponse: 'img77', help: 'Button border radius' },
				],
			},
			{
				name: 'Hard',
				points: 200,
				color: '#ff1f5d',
				questions: [
					{ choix: ['img79', 'img80'], reponse: 'img80', help: 'Subtitle typography' },
					{ choix: ['img81', 'img82'], reponse: 'img81', help: 'Text letter spacing' },
					{ choix: ['img83', 'img84'], reponse: 'img84', help: 'Activity indicator position' },
					{ choix: ['img85', 'img86'], reponse: 'img85', help: 'Icon alignment' },
					{ choix: ['img87', 'img88'], reponse: 'img87', help: 'Button height' },
					{ choix: ['img89', 'img90'], reponse: 'img89', help: 'Card shadow offset' },
					{ choix: ['img91', 'img92'], reponse: 'img91', help: 'Separator line length' },
					{ choix: ['img93', 'img94'], reponse: 'img94', help: 'Typo in text block' },
					{ choix: ['img95', 'img96'], reponse: 'img96', help: 'Search icon resolution' },
					{ choix: ['img97', 'img98'], reponse: 'img97', help: 'Button shapes' },
					{ choix: ['img99', 'img100'], reponse: 'img99', help: 'Spacing between buttons' },
					{ choix: ['img101', 'img102'], reponse: 'img102', help: 'Button caption tracking' },
					{ choix: ['img103', 'img104'], reponse: 'img104', help: 'Subtitle text separators' },
					{ choix: ['img105', 'img106'], reponse: 'img106', help: 'Skip button font weight' },
					{ choix: ['img107', 'img108'], reponse: 'img107', help: 'Title tracking' },
					{ choix: ['img109', 'img110'], reponse: 'img109', help: 'Button padding' },
					{ choix: ['img111', 'img112'], reponse: 'img112', help: 'Placeholder text baseline' },
					{ choix: ['img113', 'img114'], reponse: 'img114', help: 'Spacing between icon and text' },
				],
			},
		]
	},
	mutations: {
	},
	actions: {
	},
	modules: {
	},
});
